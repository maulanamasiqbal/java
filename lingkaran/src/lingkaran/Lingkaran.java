package lingkaran;

import java.util.Scanner;

public class Lingkaran {
    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       System.out.println("Masukan diameter lingkaran: ");
       int d = scanner.nextInt();
 
       double r = d/2;
       double phi  = 3.14;
       double L = phi * r *r;
 
       System.out.println(L);
    }
    
}
